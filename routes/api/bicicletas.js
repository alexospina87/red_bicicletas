var express=require('express');
var router=express.Router();
var bicicletaControlller=require('../../controllers/api/bicicletaControllerAPI');

router.get('/',bicicletaControlller.bicicleta_list);

///Trabajando con la API
router.post('/create',bicicletaControlller.bicicleta_create);


router.delete('/delete',bicicletaControlller.bicicleta_delete);

module.exports=router;