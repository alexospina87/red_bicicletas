var map = L.map('main_map').setView([4.6445253,-74.1395361,17], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    // maxZoom: 18,
    // id: 'mapbox/streets-v11',
    // tileSize: 512,
    // zoomOffset: -1,
    // accessToken: 'your.mapbox.access.token'
}).addTo(map);

L.marker([4.6445146,-74.1385598]).addTo(map);

// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}'

//Integracion API
$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success:function(result){
        console.log("Entro acaaaaaaa"+result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion,{title:bici.id}).addTo(map);
        });
    }
})
